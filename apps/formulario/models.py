from django.db import models


class Formulario(models.Model):
    numbers = models.BigIntegerField(verbose_name='teléfono', blank=False, null=False)
    email = models.EmailField(verbose_name='email', blank=False, null=False)
    texto = models.TextField(verbose_name='su texto aquí', blank=False, null=False)

    def __str__(self):
        return f'email: {self.email}'
