# Generated by Django 3.1.2 on 2020-10-28 21:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formulario', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formulario',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='formulario',
            name='numbers',
            field=models.BigIntegerField(verbose_name='teléfono'),
        ),
        migrations.AlterField(
            model_name='formulario',
            name='texto',
            field=models.TextField(verbose_name='su texto aquí'),
        ),
    ]
