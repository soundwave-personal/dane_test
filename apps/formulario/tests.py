from django.test import TestCase
from .models import Formulario
from .forms import FormularioForm


class FormularioTestCase(TestCase):

    def setUp(self):
        Formulario.objects.create(numbers=123444, email='test@testcase.com', texto='test case text ')


    def test_registers_found(self):
        reg = Formulario.objects.get(numbers=123444)
        self.assertEqual(reg.email, 'test@testcase.com')

    def test_form_success(self):
        form_data = {
            'numbers': 2233233,
            'email': 'test@testcase2.com',
            'texto': 'este es otro texto'
        }
        form = FormularioForm(data=form_data)
        self.assertTrue(form.is_valid())
    
    def test_form_fail(self):
        form_data = {
            'numbers': 'invalid',
            'email': 'test@testcase2.com',
            'texto': 'este es otro texto'
        }
        form = FormularioForm(data=form_data)
        self.assertFalse(form.is_valid())


    def test_form_page(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'formulario/formulario_form.html')