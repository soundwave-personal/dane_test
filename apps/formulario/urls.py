from django.urls import path
from .views import FormularioCreate, Success

app_name = 'formulario'

urlpatterns = [
    path('', FormularioCreate.as_view(), name='formulario'),
    path('success/', Success.as_view(), name='success')
]