from .forms import FormularioForm
from .models import Formulario

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView


class FormularioCreate(CreateView):
    model = Formulario
    form_class = FormularioForm
    success_url = reverse_lazy('formulario-form:success')

class Success(TemplateView):
    template_name = 'formulario/success.html'
