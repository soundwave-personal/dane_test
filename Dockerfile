FROM python:3.8-slim
ENV PYTHONUNBUFFERED 1

ADD . /web_dane
WORKDIR /web_dane

RUN apt-get update \
    && apt-get install -y gcc\
    && apt-get install -y python3-dev\
    && apt-get install -y python-mysqldb\
    && apt-get install -y default-mysql-client\
    && apt-get install -y default-libmysqlclient-dev\
    && apt-get clean

RUN pip install --upgrade pip
COPY requirements.txt /web_dane/
RUN pip install -r requirements.txt
COPY . /web_dane/
