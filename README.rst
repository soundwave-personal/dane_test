================
DANE TEST
================

to run the solution execute:

.. code:: bash

    sudo docker-compose up -d

you can access to the project using pointing a webbrowser to http://0.0.0.0:8000/
you can access to the project admin pointing a webbrowser to http://0.0.0.0:8000/admin/ with user daneweb and password dane123456


*********
Problem
*********

Maka a form that holds 3 fields:

 - a numeric field
 - an email field
 - a text field

using django with mysql database 

**************
what do I do
**************

- I Built a simple project using clase based viwes
- I wrote simple model and use it with forms to explote validation capabilities of the framework
- I built docker compose to deploy entire solution
- Wrote some test that you cand find in apps/formulario/tests.py